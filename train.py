import re

import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.python.keras import preprocessing

train_data = pd.read_json("MOD_train_data.json")
word_df = pd.read_csv("MOD_words.csv", index_col="word")

# use words that have at least 3 occurrences
word_df = word_df[word_df.occurrences >= 3]
vocab_size = len(word_df)
print("training with vocab = %d" % vocab_size)

# fix arrays to 200 elements
x_train = np.array(preprocessing.sequence.pad_sequences(
    np.array(train_data["train_words"]), maxlen=200, padding='post', value=int(word_df.loc["<PAD>"].id),
))
y_train = np.array([[l] for l in train_data["label"]])

# Train the model
# from sklearn.ensemble import RandomForestClassifier
# model = RandomForestClassifier(n_estimators=200)

# predictions = model.predict(x_val_tfidf)

# from sklearn.metrics import confusion_matrix,f1_score
# confusion_matrix(y_val,predictions)
# f1_score(y_val,predictions)

# val Data Set

# val_data = pd.read_csv('val_tweets.csv')

# Cleaning the tweets and adding it to a new column
# val_data['processed_tweet'] = val_data['tweet'].apply(process_tweet)

# Checking if a new column of processed tweets exist
# print(val_data)

# Removing the original tweet column and keeping the processed tweet column to label based on it
# drop_features(['tweet'],val_data)

# train_counts = count_vect.fit_transform(train_data['processed_tweets'])
# val_counts = count_vect.transform(val_data['processed_tweet'])

# print(train_counts.shape)
# print(val_counts.shape)

# train_tfidf = transformer.fit_transform(train_counts)
# val_tfidf = transformer.transform(val_counts)

# print(train_tfidf.shape)
# print(val_tfidf.shape)

# predictions = model.predict(val_tfidf)

# final_result = pd.DataFrame({'id':val_data['id'],'label':predictions})
# final_result.to_csv('output.csv',index=False)

if __name__ == '__main__':
    model = keras.Sequential([
        keras.layers.Embedding(vocab_size, 32, input_shape=x_train[0].shape),
        keras.layers.GlobalAveragePooling1D(),
        keras.layers.Dense(32, activation=keras.activations.relu),
        keras.layers.Dense(32, activation=keras.activations.relu),
        keras.layers.Dense(16, activation=keras.activations.relu),
        keras.layers.Dense(1, activation=tf.nn.sigmoid),
    ])
    model.summary()
    model.compile(
        optimizer=keras.optimizers.Adam(),
        loss="binary_crossentropy",
        metrics=["acc"],
    )

    model.fit(x_train, y_train, batch_size=128, epochs=10, validation_split=.35, use_multiprocessing=True, workers=12)
    model.save("model.hdf5")
