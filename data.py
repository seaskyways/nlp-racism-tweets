import pandas as pd
import re

# Importing the data to train
train_data: pd.DataFrame = pd.read_csv("train_Tweets.csv")

# Checking if the data is imported
# print(train_data)

# Checking the info of the training set
# print(train_data.info())
# Found out it have 3 columns 31962 rows and non of them have a null value

# Now time to clean the tweets from special characters, links , and users


def process_tweet(tweet):
    return " ".join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])", "", tweet.lower()).split())


train_data['processed_tweets'] = train_data['tweet'].apply(process_tweet)

# Checking that the file have processed tweets
# print(train_data)


# Checking if the file have only 2 columns left
# print(train_data)

id_counter = 0


def get_id():
    global id_counter
    id_counter = id_counter + 1
    return id_counter


# contains all words in tweets identified by a unique id
word_df = pd.DataFrame(columns=["word", "id", "occurrences"])
word_df.set_index(keys=["word"], inplace=True)
word_df.loc["<PAD>"] = ["<PAD>", get_id(), 99999]
word_df.loc["<START>"] = ["<START>", get_id(), 99999]
word_df.loc["<END>"] = ["<END>", get_id(), 99999]
word_df.loc["<UNK>"] = ["<UNK>", get_id(), 99999]


def add_word(word):
    if word in word_df.index:
        word_df.loc[word].occurrences = word_df.loc[word].occurrences + 1
    else:
        i = get_id()
        word_df.loc[word] = [word, i, 1]


def encode_str(s: str):
    return [word_df.loc[w].id if w in word_df.index else word_df.loc["<UNK>"].id for w in s.split()]


def add_words_to_dict_and_encode(row):
    words = row["processed_tweets"].split()
    for w in words:
        add_word(w)
    return encode_str(row['processed_tweets'])


# To split our training and validation datasets into 2 seperate datasets
# Splitting the data to 80% of training and 20% of validation
train_data["train_words"] = train_data.apply(add_words_to_dict_and_encode, axis=1)

train_data.to_json("./MOD_train_data.json")
word_df.to_csv("./MOD_words.csv", index=False)

if __name__ == '__main__':
    pass
