import re

import pandas as pd
from tensorflow import keras

word_df = pd.read_csv("MOD_words.csv", index_col="word")
word_df = word_df[word_df.occurrences >= 3]


def encode_str(s: str):
    return [word_df.loc[w].id if w in word_df.index else word_df.loc["<UNK>"].id for w in s.split()]


model: keras.Model = keras.models.load_model("model.hdf5")

test_data = pd.read_csv("test_tweets.csv")


def process_tweet(tweet):
    return encode_str(" ".join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])", "", tweet.lower()).split()))


x_test = test_data["tweet"].apply(process_tweet)
x_test = keras.preprocessing.sequence.pad_sequences(x_test, maxlen=200, padding="post", value=word_df.loc["<PAD>"].id)
test_data["label"] = model.predict(x_test)

test_data.to_csv("test_tweets_LABELLED.csv", index=False)

if __name__ == '__main__':
    pass
